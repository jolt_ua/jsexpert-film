import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  countdown: number;

  constructor(private router: Router) { }

  ngOnInit() {
    let seconds = 5
    let timer = setInterval(() => {
      if (seconds != 0) {
        this.countdown = seconds;
        seconds--;
        } else {
          clearInterval(timer)
          this.router.navigate(['main'])
      }
    }, 1000);
    
  }
}
