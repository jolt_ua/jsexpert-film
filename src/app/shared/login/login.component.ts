import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SessionId } from 'src/app/models/session-id.model';
import { User } from 'src/app/models/user.model';
import { FilmService } from 'src/app/film-catalog/film.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = {login: '', password: ''}
  isAlertMsg: boolean = false;
  alertMsg: string = '';
  userForm: FormGroup;
  formErrors = {
    "userName": "",
    "userPassword": ""
  }
  validationMessages = {
    "userName": {
      "required": "Обязательное поле.",
      "minlength": "Значение должно быть не менее 5 символов.",
      "maxlength": "Значение не должно быть больше 25 символов.",
      "email": "Не правильный формат email адреса."
    },
    "userPassword": {
      "required": "Обязательное поле.",
      "minlength": "Значение должно быть не менее 5 символов.",
      "maxlength": "Значение не должно быть больше 25 символов."
    }
  }

  constructor(private router: Router,
              private loginService: LoginService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.buildForm();
  }
  
  buildForm() {
    this.userForm = this.fb.group({
      userName: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(25), 
        // Validators.email
      ]],
      userPassword: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(25)
      ]]
    })
    this.userForm.valueChanges.subscribe(data => this.onValueChanges())
  }

  onValueChanges() {
    // if (!this.userForm) return;
    
    for (let item in this.formErrors) {
        this.formErrors[item] = "";
        let control = this.userForm.get(item);
        if (control && !control.valid && (control.dirty && control.touched)) {
            let message = this.validationMessages[item];
            for (let key in control.errors) {
                this.formErrors[item] += message[key] + " ";
            }
        }
    }
  }

  logging() {
    this.loginService.getSessionId(this.user)
      .subscribe((user: User) => {
        this.loginService.userId = user.id;
        localStorage.setItem('userId', `${user.id}`)
        if (user.id) {
          this.loginService.isLoggedIn();
          this.router.navigate(['main']);
        }
      },
      err => {
        if(+err.status === 401) {
          this.isAlertMsg = true;
          this.alertMsg = 'Такого пользователя не существует';
          setTimeout(() => { this.isAlertMsg = false}, 2000)
        } else {
          this.isAlertMsg = true;
          this.alertMsg = 'Повторите вашу попытку через несколько минут';
          setTimeout(() => { this.isAlertMsg = false}, 2000)
        }
      })
  }

}
