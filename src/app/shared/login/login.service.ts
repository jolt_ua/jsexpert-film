import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { concatMap, tap } from 'rxjs/operators';

import { CUSTOM_CONFIG } from '../config';
import { Config } from 'src/app/models/config.model';
import { RequestToken } from 'src/app/models/token.model';
import { SessionId } from 'src/app/models/session-id.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn$ = new Subject<boolean>();
  isLogIn: boolean = false;
  requestToken: string;
  sessionId: string; 
  userId: number;
  

  constructor(@Inject(CUSTOM_CONFIG) public config: Config,
              private http: HttpClient)  {
                            
    this.isLogIn = !!localStorage.getItem('auth_token')
  }

  getSessionId(user) {
    return this.http.get(`${this.config.tokenUrl}`).pipe(
      concatMap((token: RequestToken) => {
        this.requestToken = token.request_token;
        return this.http.get(
          `${this.config.validateUrl}username=${user.login}&password=${user.password}&request_token=${token.request_token}&api_key=${this.config.apiKey}`);
      }),
      concatMap((token: RequestToken) => {
        return this.http.get(
          `${this.config.sessionUrl}&request_token=${token.request_token}`
        );
      }),
      concatMap((session: SessionId) => {
        this.sessionId = session.session_id;
        localStorage.setItem('sessionId', `${this.sessionId}`);
        return this.http.get(`${this.config.accountUrl}?api_key=${this.config.apiKey}&session_id=${session.session_id}`)
          // .pipe(
          //   tap(res => this.user = res)
          // )
      })
    );
  }

  getUserData() {

  }

  validationStorage(user) {
    console.log(user)
    if (user.userName.value != localStorage.getItem('login')) {
      this.isLoggedOut();
      return {status: false, msg: 'Неверное имя'}
    } else if (user.userPassword.value != localStorage.getItem('password')) {
      this.isLoggedOut();
      return {status: false, msg: 'Неверный пароль'}
    } else {
      this.isLoggedIn();
      return {status: true, msg: 'Все верно'}
    }
  }

  getLoggedIn() {
    return this.isLoggedIn$.asObservable();
  }

  isLoggedIn() {
    this.isLogIn = true;
    localStorage.setItem('auth_token', 'islogin');
    this.isLoggedIn$.next(true)
  }

  isLoggedOut() {
    this.isLogIn = false;
    localStorage.removeItem('auth_token');
    localStorage.removeItem('sessionId');
    localStorage.removeItem('userId');
    this.isLoggedIn$.next(false)
  }

  getLogIn() {
    return this.isLogIn
  }
}
