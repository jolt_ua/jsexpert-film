import { InjectionToken } from "@angular/core";
import { Config } from "src/app/models/config.model";

const configConst = {
  apiUrl: 'https://api.themoviedb.org/3',
  // apiKey: '0994e7679a856150aadcecf7de489bce',
  apiKey: '5f6e2060f7f99c36c1d8f94395b305bd',
  imgPath: 'https://image.tmdb.org/t/p',
  localApiUrl: "http://localhost:3000",
}

export const config: Config = {
  apiKey: `${configConst.apiKey}`,
  apiUrl: `${configConst.apiUrl}`,
  accountUrl: `${configConst.apiUrl}/account`,
  movieUrl: `${configConst.apiUrl}/movie`,
  searchUrl: `${configConst.apiUrl}/search`,
  personUrl: `${configConst.apiUrl}/person`,
  validateUrl: `${configConst.apiUrl}/authentication/token/validate_with_login?`,
  tokenUrl: `${configConst.apiUrl}/authentication/token/new?api_key=${configConst.apiKey}`,
  sessionUrl: `${configConst.apiUrl}/authentication/session/new?api_key=${configConst.apiKey}`,
  params: `api_key=${configConst.apiKey}&language=ru-RU`,
  

  midImgPath: `${configConst.imgPath}/w500`,
  smallImgPath: `${configConst.imgPath}/w185`,
  bigBackPath: `${configConst.imgPath}/w1280`,
  midBackPath: `${configConst.imgPath}/w780`,
  smallBackPath: `${configConst.imgPath}/w300`,

  localApiUrl: `${configConst.localApiUrl}`,
  favoritesApiUrl: `${configConst.localApiUrl}/films/favorites`,
  bookmarkApiUrl: `${configConst.localApiUrl}/films/bookmark`
}

export const CUSTOM_CONFIG = new InjectionToken<Config>('qwerty');

