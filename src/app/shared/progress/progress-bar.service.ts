import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ProgressBarService {

  progressBar$ = new Subject; 

  constructor() { }

  getProgressBarStatus() {
    return this.progressBar$.asObservable();
  }

  show() {
    this.progressBar$.next(true);
  }

  hide() {
    this.progressBar$.next(false);
  }

}
