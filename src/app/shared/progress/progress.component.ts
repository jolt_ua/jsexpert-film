import { Component } from '@angular/core';
import { ProgressBarService } from './progress-bar.service';


@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent {
  isShow = false;
  constructor(public progressBarService: ProgressBarService ) {
    this.progressBarService.getProgressBarStatus().subscribe((is: boolean) => this.isShow = is)
  }
}