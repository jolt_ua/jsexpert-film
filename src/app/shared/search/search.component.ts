import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FilmService } from '../../film-catalog/film.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  value: string = '';
  isHide: boolean = true;

  constructor(public router: Router,
              public filmService: FilmService) { }

  ngOnInit() {
    
  }

  sendValueToService() {
    if ( this.value.length >= 3) {
      this.filmService.searchValue = this.value
      this.router.navigate(['search-result']);

    } else if (this.value === '') {
      this.filmService.searchValue = ''
    }
}

clearValue(){ //TODO
  this.value = '';
}

}
