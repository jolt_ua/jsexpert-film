import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FilmCatalogModule } from './film-catalog/film-catalog.module';
import { AppMaterialModule } from './modules/app-material.module';
import { ProgressComponent } from './shared/progress/progress.component';
import { CUSTOM_CONFIG, config } from './shared/config';
import { SearchComponent } from './shared/search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './shared/login/login.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';



@NgModule({
  declarations: [
    AppComponent,
    ProgressComponent,
    SearchComponent,
    LoginComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FilmCatalogModule,
    AppMaterialModule,
    MatProgressBarModule,
    FormsModule,
    ReactiveFormsModule 
    
  ],
  providers: [{provide: CUSTOM_CONFIG, useValue: config}],
  bootstrap: [AppComponent]
})
export class AppModule { }

