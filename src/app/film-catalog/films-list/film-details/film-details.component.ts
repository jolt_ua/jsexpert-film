import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FilmService } from '../../film.service';
import { CUSTOM_CONFIG } from '../../../shared/config';
import { Config } from 'src/app/models/config.model';
import { CreditsResponce } from 'src/app/models/credits-responce.model';
import { FilmDetails } from 'src/app/models/film-details.model';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { VideoResults } from 'src/app/models/video-results.model';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.css']
})
export class FilmDetailsComponent implements OnInit {

  details: object;
  imgUrl: string;
  credits: Object;
  myBackgroundUrl: string;
  creators: object[];
  safeSrc: SafeResourceUrl;

  constructor(private activatedRoute: ActivatedRoute,
              private filmService: FilmService,
              @Inject(CUSTOM_CONFIG) public config: Config,
              private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.imgUrl = `${this.config.midImgPath}`;
    this.activatedRoute.paramMap.subscribe((params: Params) => {
      const id = +params.get('id');
      this.filmService.getVideo(id).subscribe((res: VideoResults) => {
        console.log(res);
        const key: string = res.results[0].key;
        this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl(`https://www.youtube.com/embed/${key}`)
      });
      this.filmService.getFilmDetails(id).subscribe((details: FilmDetails) => {
          console.log(details, 'details');
          this.details = details;
          this.myBackgroundUrl = `${this.config.midImgPath}${details.backdrop_path}`;
        });
      this.filmService.getFilmCredits(id).subscribe((credits: CreditsResponce) => {
          console.log(credits, 'credits');
          this.credits = credits.cast.slice(0, 5);
          this.creators = credits.crew.slice(0, 3);
        })
      });
      
           
      
      console.log(this.details, 'is this.details');  
    
  }

  

}

