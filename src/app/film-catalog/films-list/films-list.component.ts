import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';

import { LoginService } from 'src/app/shared/login/login.service';

@Component({
  selector: 'app-films-list',
  templateUrl: './films-list.component.html',
  styleUrls: ['./films-list.component.css']
})

export class FilmsListComponent implements OnInit {

  items: any[];
  isMaxPage: boolean = false;
  currentPage: number = 1;
  sortingType: string = 'movie';
  isActorHandler: boolean = false;
  isHideNextBtn: boolean = true;
  isNotFound: boolean;
  favList: number[] = [];
 
  constructor(public filmsService: FilmService,
              private loginService: LoginService) { }
    
  ngOnInit() {
    // this.filmsService.showSearch(); 
    this.filmsService.searchParam = 'movie';
    this.filmsService.getNextPage(this.currentPage, this.sortingType).subscribe(
      (filmList: any) => {
        this.items = filmList.results;
      },
      err => {
        console.log("error");
      });
    setTimeout(() => {
      this.isHideNextBtn = false;      
    }, 2000)
    this.getAllFavoriteFilms()
  }

  getNextPageHandler(){ 
    this.filmsService.getNextPage(this.currentPage + 1, this.sortingType)
      .subscribe((filmList: any) => {
          this.items = [...this.items].concat(filmList.results);
          this.setIsFavorite();
          if (filmList.total_pages === this.currentPage) {
            this.isMaxPage = true;
          }
        },
        err => {
          console.log("error");
        })
      this.currentPage++; 
  }

  getAllFavoriteFilms() {
    this.filmsService.getAllFavoriteFilms().subscribe(res => {
        res.map(page => page.results).map(arr => {
          arr.map(item => this.favList.push(item.id))
        })
        this.setIsFavorite();
    }, err => {
      if(+err.status === 401) {
        this.loginService.isLoggedOut();
      }
    })
  }

  setIsFavorite() {
    this.items.map(item => item.isFavorite = this.favList.indexOf(item.id) > -1)
  }

  setFavoriteStatus(id: number){ 
    this.filmsService.editFavoriteStatus(id, true).subscribe(res => {
      console.log(res, 'result');
      this.favList.push(id); //можно через запрос всего списка избранного
      this.setIsFavorite();
    }, err => {
      if(+err.status === 401) {
        this.loginService.isLoggedOut();
      }
    })
  }

  deleteFavoriteStatus(id: number) {
    this.filmsService.editFavoriteStatus(id, false).subscribe(res => {
      // let indx = this.favList.indexOf(id);
      // this.favList.splice(indx, 1);
      this.favList = this.favList.filter(item => item != id);
      this.setIsFavorite();
    }, err => {
      if(+err.status === 401) {
        this.loginService.isLoggedOut();
      }
    })
  }

}
