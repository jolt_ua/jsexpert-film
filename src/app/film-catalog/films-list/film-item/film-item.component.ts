import { Component, OnInit, Input, EventEmitter, Output, Inject } from '@angular/core';
import { FilmService } from '../../film.service';
import { CUSTOM_CONFIG } from '../../../shared/config';
import { Config } from 'src/app/models/config.model';
import { Router } from '@angular/router';



@Component({
  selector: 'app-film-item',
  templateUrl: './film-item.component.html',
  styleUrls: ['./film-item.component.css']
})
export class FilmItemComponent implements OnInit {

  @Input() item;
  @Output() updateFavorite = new EventEmitter<number>();
  @Output() deleteFavorite = new EventEmitter<number>();
  @Output() updateBookmark = new EventEmitter<number>();

  imgUrl: string;

  constructor(public filmsService: FilmService,
              @Inject(CUSTOM_CONFIG) public config: Config, 
              private router: Router
    ) { }
  
  ngOnInit() {   
    this.imgUrl = `${this.config.midImgPath}`
  }
  
  setToFavorite() {
    this.updateFavorite.emit(this.item.id);
  }

  setToBookmark() {
    this.updateBookmark.emit(this.item.id);
  }

  showDetails() {
    this.router.navigate(['/film', this.item.id])
  }

  deleteFavoriteStatus() {
    this.deleteFavorite.emit(this.item.id)
  }
}


