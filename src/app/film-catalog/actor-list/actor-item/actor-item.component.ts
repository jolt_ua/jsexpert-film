import { Component, OnInit, Input, Inject } from '@angular/core';
import { FilmService } from '../../film.service';
import { Config } from 'src/app/models/config.model';
import { CUSTOM_CONFIG } from '../../../shared/config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-actor-item',
  templateUrl: './actor-item.component.html',
  styleUrls: ['./actor-item.component.css', '../../films-list/film-item/film-item.component.css']
})
export class ActorItemComponent implements OnInit {

  @Input() actor;
  imgUrl: string;
  
  constructor(public filmsService: FilmService,
              @Inject(CUSTOM_CONFIG) public config: Config,
              private router: Router) { }

  ngOnInit() {
    this.imgUrl = `${this.config.midImgPath}`
  }

  showDetails() {
    this.router.navigate(['/actor', this.actor.id])
  }
}
