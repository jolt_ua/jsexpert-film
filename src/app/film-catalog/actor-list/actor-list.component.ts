import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import { Actor } from 'src/app/models/actor.model';

@Component({
  selector: 'app-actor-list',
  templateUrl: './actor-list.component.html',
  styleUrls: ['./actor-list.component.css']
})
export class ActorListComponent implements OnInit {

  actors: Actor[];
  currentPage: number = 1;
  sortingType: string = 'person'

  constructor(public filmsService: FilmService) { }

  ngOnInit() {
    // this.filmsService.showSearch();
    this.filmsService.searchParam = 'person';
    this.filmsService.getNextPage(this.currentPage, this.sortingType).subscribe(
      (actorList: any) => {
        this.actors = actorList.results;
        // this.getFavoriteStatus();
        // this.getBookmarkStatus()
      },
      err => {
        console.log("error");
      });
  }

  

}
