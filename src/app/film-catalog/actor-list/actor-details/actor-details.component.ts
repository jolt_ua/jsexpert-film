import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FilmService } from '../../film.service';
import { CUSTOM_CONFIG } from '../../../shared/config';
import { Config } from 'protractor';
import { CreditsResponce } from 'src/app/models/credits-responce.model';
import { switchMap } from 'rxjs/operators';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-actor-details',
  templateUrl: './actor-details.component.html',
  styleUrls: ['./actor-details.component.css',
              '../../films-list/film-details/film-details.component.css']
})
export class ActorDetailsComponent implements OnInit {
  
  details: object;
  imgUrl: string;
  credits: object[];
  isFullBiography: boolean = false;
  editBiography: string;

  constructor(private activatedRoute: ActivatedRoute,
              private filmService: FilmService,
              @Inject(CUSTOM_CONFIG) public config: Config,) { }

  ngOnInit() {
    // this.imgUrl = `${this.config.midImgPath}`
    // this.activatedRoute.paramMap.subscribe((params: Params) => {
    //   const id = +params.get('id');
    //   this.filmService.getActorDetails(id).subscribe((details) => {
    //       this.details = details;
    //     })
    //   this.filmService.getActorCredits(id).subscribe((credits: CreditsResponce) => {
    //       this.credits = credits.cast.slice(0, 5)
    //       console.log(credits);
    //     })
    //   }) 

    this.imgUrl = `${this.config.midImgPath}`
    this.activatedRoute.paramMap.pipe(
      switchMap((params: Params) => {
        let id = +params.get('id');
        return forkJoin(this.filmService.getActorDetails(id), this.filmService.getActorCredits(id))
      })
    ).subscribe((results: any) => {
      this.details = results[0];
      this.credits = results[1].cast.slice(0, 5);
    })
  }

  showFullBiography(event) {
    event.preventDefault();
    if (!this.isFullBiography) {
      this.isFullBiography = true;
    } else {
      this.isFullBiography = false;
    }
  }
}
