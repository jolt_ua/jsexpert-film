import { Injectable, Inject } from '@angular/core'; 
import { HttpClient } from '@angular/common/http';
import { ProgressBarService } from '../shared/progress/progress-bar.service';
import { delay, tap, pluck, map, switchMap } from 'rxjs/operators';
import { CUSTOM_CONFIG } from '../shared/config';
import { Config } from '../models/config.model';
import { BehaviorSubject, from, forkJoin } from 'rxjs';
import { LoginService } from '../shared/login/login.service';

@Injectable({
  providedIn: 'root'
})

export class FilmService {
  searchValue: string;
  searchType: string;
  searchParam: string;
  showSearch$ = new BehaviorSubject<boolean>(true);
  favoriteTotalPages: number = 1;
  

  constructor( 
    private http: HttpClient,
    public progressBarService: ProgressBarService,
    @Inject(CUSTOM_CONFIG) public config: Config
    ) {}
    
    getNextPage (page?: number, value?: string) {
      this.progressBarService.show();
      return this.http.get(`${this.config.apiUrl}/${value}/popular?page=${page}&${this.config.params}`).pipe(
        tap(() => {
          this.progressBarService.hide();
        })
        )
      }
    
    getSearchedFilms(page: number) {
      this.progressBarService.show();
      return this.http.get(`${this.config.searchUrl}/${this.searchParam}?page=${page}&${this.config.params}&include_adult=false&query=${this.searchValue}`).pipe(
        tap(() => {
          this.progressBarService.hide();
        })
      )
    }

    getFilmDetails(id: number) {
      this.progressBarService.show();
      return this.http.get(`${this.config.movieUrl}/${id}?${this.config.params}`).pipe(
        tap(() => {
          this.progressBarService.hide()
        })
      )
    }

    getActorDetails(id: number) {
      this.progressBarService.show();
      return this.http.get(`${this.config.personUrl}/${id}?${this.config.params}`).pipe(
        tap(() => {
          this.progressBarService.hide()
        })
      )
    }
    getFilmCredits(id: number) {
      this.progressBarService.show();
      return this.http.get(`${this.config.movieUrl}/${id}/credits?${this.config.params}`).pipe(
        tap(() => {
          this.progressBarService.hide()
        })
      )
    }
    getActorCredits(id: number) {
      this.progressBarService.show();
      return this.http.get(`${this.config.personUrl}/${id}/movie_credits?${this.config.params}`).pipe(
        tap(() => {
          this.progressBarService.hide()
        })
      )
    }

    getVideo(id: number) {
      this.progressBarService.show();
      return this.http.get(`${this.config.movieUrl}/${id}/videos?${this.config.params}`).pipe(
        tap(() => {
          this.progressBarService.hide()
        })
      )
    }

    getAllFavoriteFilms() {
      return this.getFavorite(this.favoriteTotalPages).pipe(
        switchMap((y:any) => {
          this.favoriteTotalPages = y.total_pages;
          const taskQueue =[];
          for(let i =1; i < this.favoriteTotalPages + 1; i++) {
            taskQueue.push(from(this.getFavorite(i)));
          }
          return forkJoin(...taskQueue);
        })
      )
    }

    getFavorite(page) {
      return this.http.get(`${this.config.accountUrl}/${localStorage.getItem('userId')}/favorite/movies?api_key=${this.config.apiKey}&session_id=${localStorage.getItem('sessionId')}&page=${page}`)
       
    }

    editFavoriteStatus(id, status) {
      return this.http.post(`${this.config.accountUrl}/${localStorage.getItem('userId')}/favorite?api_key=${this.config.apiKey}&session_id=${localStorage.getItem('sessionId')}`, 
        {
          "media_type": "movie",
          "media_id": id,
          "favorite": status
        })
    }
    
    
    // getShowSearch() {
    //   return this.showSearch$.asObservable();
    // }

    // showSearch() {
    //   this.showSearch$.next(true);
    // }

    // hideSearch() {
    //   this.showSearch$.next(false);
    // }
    
}







