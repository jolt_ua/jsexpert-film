import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import { ProgressBarService } from 'src/app/shared/progress/progress-bar.service';
import { Router } from '@angular/router';
import { Actor } from 'src/app/models/actor.model';
import { Film } from 'src/app/models/film.model';
import { ActorsResponce } from 'src/app/models/actors-responce.model';
import { FilmsResponce } from 'src/app/models/films-responce.model';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css', 
              '../films-list/films-list.component.css']
})
export class SearchResultComponent implements OnInit {

  value: string = 'movie';
  quary: string = '';
  page: number = 1;
  actors: Actor[];
  films: Film[];
  isActorHandler: boolean = false;
  totalResult: number;
  currentPage: number = 1;
  isMaxPage: boolean = false;
  searchParam: string;
  
  constructor(public filmsService: FilmService,
              public progressBarService: ProgressBarService,
              private router: Router) { 
      
    }

  ngOnInit() { 
    // this.filmsService.hideSearch();
    this.searchParam = this.filmsService.searchParam
    if (this.searchParam === 'person') {
      this.isActorHandler = true
      this.filmsService.getSearchedFilms(this.page).subscribe(
        (searchList: ActorsResponce) => {
          this.actors = searchList.results;
          this.totalResult = searchList.total_results;
          this.progressBarService.hide();
      },
      err => {
        console.log("error");
      })
    }
    if (this.searchParam === 'movie') {
      this.isActorHandler = false;
      this.filmsService.getSearchedFilms(this.page).subscribe(
        (searchList: FilmsResponce) => {
          this.films = searchList.results;
          this.totalResult = searchList.total_results;
          this.progressBarService.hide();
      },
      err => {
        console.log("error");
      })
    }
  }

  getNextPageHandler(){ 
    if (this.searchParam === 'person') {
      this.filmsService.getSearchedFilms(this.currentPage + 1).subscribe(
        (searchList: ActorsResponce) => {
          this.actors = [...this.actors].concat(searchList.results);
          if (searchList.total_pages === this.currentPage) {
            this.isMaxPage = true;
          }
        },
        err => {
          console.log("error");
        })
      this.currentPage++; 
    }
    if (this.searchParam === 'movie') {
      this.filmsService.getSearchedFilms(this.currentPage + 1).subscribe(
        (searchList: FilmsResponce) => {
          this.films = [...this.films].concat(searchList.results);
          if (searchList.total_pages === this.currentPage) {
            this.isMaxPage = true;
          }
        },
        err => {
          console.log("error");
        })
      this.currentPage++; 
    }

  }

  routeToFilmList(){
    this.router.navigate(['films-list']);
  }
    
}
