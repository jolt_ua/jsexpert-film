import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


import { MainComponent } from './main/main.component';
import { FilmsListComponent } from './films-list/films-list.component';
import { FilmItemComponent } from './films-list/film-item/film-item.component';
import { ActorItemComponent } from './actor-list/actor-item/actor-item.component';

import { AppMaterialModule } from '../modules/app-material.module';
// import { SearchComponent } from './films-list/search/search.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { ActorListComponent } from './actor-list/actor-list.component';
import { FilmDetailsComponent } from './films-list/film-details/film-details.component';
import { ActorDetailsComponent } from './actor-list/actor-details/actor-details.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppMaterialModule,
    HttpClientModule,
    RouterModule
    
  ],
  declarations: [
    MainComponent,
    FilmItemComponent,
    FilmsListComponent,
    ActorItemComponent,
    // SearchComponent,
    SearchResultComponent,
    ActorListComponent,
    FilmDetailsComponent,
    ActorDetailsComponent
  ]
})
export class FilmCatalogModule { }
