import { Component, OnInit, Inject } from '@angular/core';

import { map } from 'rxjs/operators';

import { FilmService} from '../film.service';
import { CUSTOM_CONFIG } from '../../shared/config';
import { Config } from 'src/app/models/config.model';
import { Film } from 'src/app/models/film.model';
import { Actor } from 'src/app/models/actor.model';
import { FilmsResponce } from 'src/app/models/films-responce.model';
import { ActorsResponce } from 'src/app/models/actors-responce.model';
import { Router } from '@angular/router';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {
  imgUrl: string;
  films: Film[];
  actors: Actor[];
  
  constructor(public filmsService: FilmService,
              @Inject(CUSTOM_CONFIG) public config: Config,
              private router: Router) { 
  }

  ngOnInit() {
    // this.filmsService.hideSearch(); 
    this.imgUrl = `${this.config.midImgPath}`;
    this.filmsService.getNextPage(1, 'movie').pipe(
      map((filmsList: FilmsResponce) => {return filmsList.results})
      ).subscribe(
        (filmsList: Film[]) => {
          this.films = filmsList.splice(0, 6);
        },
        err => {
          console.log("error");
        });

    this.filmsService.getNextPage(1, 'person').pipe(
      map((actorsList: ActorsResponce) => {return actorsList.results})
      ).subscribe(
        (actorsList: Actor[]) => {
          this.actors = actorsList.splice(0, 6);
          
      },
      err => {
        console.log("error");
      });
  }

  
  
}
