import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './film-catalog/main/main.component';
import { FilmsListComponent } from './film-catalog/films-list/films-list.component';
import { SearchResultComponent } from './film-catalog/search-result/search-result.component';
import { ActorListComponent } from './film-catalog/actor-list/actor-list.component';
import { LoginComponent } from './shared/login/login.component';
import { FilmDetailsComponent } from './film-catalog/films-list/film-details/film-details.component';
import { AuthGuard } from './shared/guards/auth-guard.service';
import { ActorDetailsComponent } from './film-catalog/actor-list/actor-details/actor-details.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';


const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "login"}, 
  { path: "login", component: LoginComponent },
  { path: "main", component: MainComponent, canActivate: [AuthGuard]},
  { path: "films-list", component: FilmsListComponent, canActivate: [AuthGuard]},
  { path: "search-result", component: SearchResultComponent, canActivate: [AuthGuard]},
  { path: "actor-list", component: ActorListComponent, canActivate: [AuthGuard]},
  { path: "film/:id", component: FilmDetailsComponent, canActivate: [AuthGuard]},
  { path: "actor/:id", component: ActorDetailsComponent, canActivate: [AuthGuard]},
  { path: "**", component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
