import { Component } from '@angular/core';
import { FilmService } from './film-catalog/film.service';
import { LoginService } from './shared/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  isShowSearch: boolean = true;
  isShowButton: boolean = false;
  links: object[] = [
    { path: '/main', label: 'Главная', active: 'button-active', icon: 'home'}, 
    { path: '/films-list', label: 'Фильмы', active: 'button-active', icon: 'movie'},
    { path: '/actor-list', label: 'Актеры', active: 'button-active', icon: 'people'}
  ];
  

  constructor(private filmsService: FilmService,
              private loginService: LoginService,
              private router: Router) {}  

  ngOnInit() {
    // this.filmsService.getShowSearch()
    //   .subscribe((res: boolean) => this.isShowSearch = res);

    this.isShowButton = this.loginService.isLogIn
     
    this.loginService.getLoggedIn()
      .subscribe((res: boolean) => this.isShowButton = res);
    
  }

  logout() {
    this.loginService.isLoggedOut();
    this.router.navigate(['login'])
  }
}

