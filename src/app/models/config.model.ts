export class Config {
    
        apiUrl: string;
        apiKey: string;
        accountUrl: string;
        movieUrl: string;
        searchUrl: string;
        personUrl: string;
        params: string;
        validateUrl: string;
        tokenUrl: string;
        sessionUrl: string;
        
        // imgPath: string;
        midImgPath: string;
        smallImgPath: string;
        bigBackPath: string;
        midBackPath: string;
        smallBackPath: string;
      
        localApiUrl: string;
        favoritesApiUrl: string;
        bookmarkApiUrl: string
      
}