import { Film } from "./film.model";

export class FilmsResponce {
    page: number;
    results: Array<Film>
    total_pages: number;
    total_results: number;
}