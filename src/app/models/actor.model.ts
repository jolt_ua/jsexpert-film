export class Actor {
    adult: boolean;
    id: number;
    know_for: Array<object>;
    name: string;
    popularity: number;
    profile_path: string;
}