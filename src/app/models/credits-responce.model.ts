export class CreditsResponce {
    id: number;
    cast: Array<object>;
    crew: Array<object>;
}