import { Actor } from "./actor.model";

export class ActorsResponce {
    page: number;
    results: Array<Actor>
    total_pages: number;
    total_results: number;

}